import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UsersServiceObs } from '../usersobs.service';


@Component({
  selector: 'app-userobs',
  templateUrl: './userobs.component.html',
  styleUrls: ['./userobs.component.css']
})
export class UserobsComponent implements OnInit {
  id:number;


  constructor(private route: ActivatedRoute, private usersService: UsersServiceObs) { }

  ngOnInit() {
    this.route.params
    .subscribe(
      (params: Params) => {
        this.id = +params['id'];
      }
    );
  }
  onActivate(){
    this.usersService.userActivated.next(this.id);

  }

}
