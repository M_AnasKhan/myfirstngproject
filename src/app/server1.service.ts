import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import 'rxjs/Rx';
@Injectable()
export class Server1Serivce{
    constructor(private http: Http){}
    storeServers(servers1: any[]) {
        const headers = new Headers({'Content-type': 'application/json'})
    // return this.http.post('https://anas-ng-http.firebaseio.com/data.json', servers1, {headers: headers});
    
    return this.http.put('https://anas-ng-http.firebaseio.com/data.json', servers1, {headers: headers});
    }
    getServers(){
        return this.http.get('https://anas-ng-http.firebaseio.com/data.json')
        .map(
            (response: Response) => {
                const data = response.json();
                for (const server of data){
                    server.name = 'Fetched_' + server.name;
                }
                return data;
            }
        );
    }

}