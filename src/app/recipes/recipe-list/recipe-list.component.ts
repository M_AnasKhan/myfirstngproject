import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('A test Recipe', 'This is simply a test', 'https://torange.biz/photo/6/IMAGE/eggs-egg-chicken-flamenco-recipes-julia-6050.jpg'),
new Recipe('A test Recipe2', 'This is simply a test2', 'https://torange.biz/photo/6/IMAGE/eggs-egg-chicken-flamenco-recipes-julia-6050.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
