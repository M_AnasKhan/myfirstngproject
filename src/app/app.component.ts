import { Component, OnInit, ViewChild } from '@angular/core';
import { AccountsService } from './accounts.service';
import { UsersServiceObs } from './usersobs.service';
import { NgForm, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';
import { Server1Serivce } from './server1.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],


})
export class AppComponent implements OnInit {
  // HTTP REQUEST
  servers1 = [
    {
      name: 'TestServer',
      capacity: 10,
      id: this.generateID()
    },
    {
      name: 'LiveServer',
      capacity: 100,
      id: this.generateID()
    }
  ];
  onAddServer(name: string){
    this.servers1.push({
      name:name,
      capacity:50,
      id: this.generateID()
    });
  }

  onSave(){
    this.server1Service.storeServers(this.servers1)
    .subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    );
  }

  onGet(){
    this.server1Service.getServers()
    .subscribe(
      (servers1: any[]) => this.servers1 = servers1,
      (error) => console.log(error)
    );
  }
  private generateID(){
    return Math.round(Math.random() * 10000);
  }

  // Reactive approach form handling
  genders_ra = ['male','female'];
  signupForm_ra: FormGroup;
  // Template driven form handling
@ViewChild('f') signupForm: NgForm;
answer = '';
genders = ['male', 'female'];
user = {
  username:'',
  email:'',
  secretQuestion:'',
  answer:'',
  gender:'',
};
submitted = false;

  oddNumbers = [1, 3, 5];
  evenNumbers = [2, 4];
  onlyOdd = false;
  value = 10;
  user1Activated = false;
  user2Activated = false;
  suggestUserName(){
    const suggestedName = 'Superuser';
    // this.signupForm.setValue({
    //   userData:{
    //     username: suggestedName,
    //     email: ''
    //   },
    //   secret: 'pet',
    //   questionAnswer: '',
    //   gender: 'male'

    // });
    this.signupForm.form.patchValue({
      userData:{
        username: suggestedName
      }
    });
  }
  // onSubmit(form: NgForm){
  //   console.log(form);
  // }

  onSubmit(){
    this.submitted = true;
    this.user.username = this.signupForm.value.userData.username;
    this.user.email = this.signupForm.value.userData.email;
    this.user.secretQuestion = this.signupForm.value.secret;
    this.user.answer = this.signupForm.value.questionAnswer;
    this.user.gender = this.signupForm.value.gender;
    this.signupForm.reset();

  }


  accounts: {name; string, status: string}[] = [];

  constructor(private accountsService: AccountsService, private usersService: UsersServiceObs, private server1Service: Server1Serivce){}
  ngOnInit(){
    //this.accounts = this.accountsService.accounts;
    this.usersService.userActivated.subscribe(
      (id: number) => {
        if (id === 1) {
          this.user1Activated = true;
        } else if (id === 2) {
          this.user2Activated = true;
        }

        
      }
    )

    this.signupForm_ra = new FormGroup({
      'userData_ra': new FormGroup({
        'username_ra': new FormControl(null, Validators.required),
        'email_ra': new FormControl(null, [Validators.required, Validators.email]) 
      }),
       'gender_ra': new FormControl('male'),
       'hobbies': new FormArray([])

    });
  }
  onSubmit_ra(){
    console.log(this.signupForm_ra);
  }

  onAddHobby(){
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm_ra.get('hobbies')).push(control);
  }
}
